# 前端部署脚本

这是项目的前端部署脚本

# 环境
node 6.x.x、git、svn

# 操作
## 克隆并移动到代码目录
```bash
$ git clone https://bitbucket.org/xing-zhi/deploy.git
$ cd deploy
```

## 安装部署脚本运行所需依赖
```bash
$ npm i
```

## 转换为命令行工具
```bash
$ npm link
```
这是就可以在任何目录下使用`deploy`这个命令了。可以使用`deploy help`查看帮助（还需进一步完善）。

## 编译前端代码
### 生成配置模板文件
在一个空白目录下运行

```bash
$ deploy init
```

或者在任何目录下运行，同时指定一个目录名

```bash
$ deploy init <folder-name>
```

### 编译前端代码
```bash
$ deploy build
```

编译的代码会移动到命令运行的目录中，后续移动需要另外写脚本（也可以考虑集成到这里）。
