#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const yargs = require('yargs');

const shell = require('shelljs');

const pathResolve = (...args) => path.resolve(__dirname, ...args);

// help
const argv = yargs.usage('$0 <cmd> [args]')
  .command('help [name]', '获取帮助信息', {
    name: {
      default: 'init'
    }
  }, function(argv) {
    console.log(argv);
  })
  .command('init [folder]', '生成配置文件模板', {
    folder: {
      // 默认是运行的目录
      default: ''
    }
  }, function(argv) {
    const info = '使用配置文件编译前端代码，使用init指令生成配置文件模板';
    console.log(info);
  })
  .command('build', '编译代码，编译的代码在当前目录', {

  }, function(argv) {
    console.log('build');
  })
  .help()
  .argv;
// \help

const command = argv._[0];

// init
if ( command === 'init' ) {
  const currentFolder = process.cwd();
  const folder = argv.folder;
  const target = path.resolve(currentFolder, folder);
  const exists = fs.existsSync(target);

  // 没有，创建并进入目录
  if ( !exists ) {
    shell.mkdir(target);
    shell.cd(target);
    writeConfigFile();
  } else {        // 有，检测是否是目录且是否为空
    const fsState = fs.lstatSync(target);

    if ( fsState.isDirectory() ) {
      const isEmpty = shell.ls(target).stdout.length === 0;

      if ( isEmpty ) {    // 目标目录为空时，进入目录
        shell.cd(target);
        writeConfigFile();
      } else {
        console.error('目标目录必须为空');
        process.exit(1);
      }
    } else {
      console.error('目标目录必须是一个目录');
      process.exit(1);
    }
  }

  // 已经进入目标目录，生成配置文件模板
  function writeConfigFile() {
    const tmplatePath = pathResolve('config.json.js');

    shell.cp(tmplatePath, process.cwd());
  }
  process.exit(0);
}
// \init

// build
if ( command === 'build' ) {
  const config = require('./config.json.js');

  // 命令运行的目录，操作以此为参考
  const pwd = shell.pwd().stdout;

  console.log('开始克隆代码');
  const feSourceFolder = 'fe';
  const vcsType = config.vcsType;
  const cloneCommandMap = {
    git: 'git clone',
    svn: 'svn checkout'
  };
  const cloneCommand = cloneCommandMap[vcsType];

  if ( cloneCommand ) {
    shell.exec(`${cloneCommand} ${config.repository} ${feSourceFolder}`);
    console.log('代码克隆完毕');
    shell.cd('fe');
  } else {
    console.log('版本控制系统只支持git或svn，请修改配置文件');
    exit();
  }

  // 编译前端代码
  const frontEndBuildFolder = path.resolve(pwd, feSourceFolder, config.frontEndBuildFolder)
  const buildFrontEnd = config.buildFrontEnd;

  shell.cd(frontEndBuildFolder);
  if ( buildFrontEnd ) {
    shell.exec(buildFrontEnd);
    console.log('前端代码编译完成');
  } else {
    console.log('前端代码无需编译');
  }
  // \build

  // 编译的代码移动到顶层目录
  const distFolder = path.resolve(pwd, feSourceFolder, config.distFolder);

  shell.mv(distFolder, pwd);

  process.exit(0);
}

if ( command !=== 'help' ) {
  console.log('请使用depoly help查看使用帮助');
}
