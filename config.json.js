// 部署配置文件，本质上是一个json文件
// 文件类型选择js是为了方便添加注释

module.exports = {
  // 版本控制系统类型：git或者svn
  vcsType: 'git',
  // 前端代码仓库的地址
  repository: 'https://xing-zhi@bitbucket.org/xing-zhi/deploy-demo-fe.git',
  // 编译前端代码需要切换的路径，相对于前端代码根目录的路径
  frontEndBuildFolder: '',
  // 编译前端端代码的命令，直接在命令行运行的命令，用shell.exec包裹，这样可以更通用
  buildFrontEnd: 'npm i && npm run release',
  // 编译后的代码相对于前端代码根目录的路径
  distFolder: 'dist'
};
